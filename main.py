import requests
import json
from bs4 import BeautifulSoup, Comment

SCORE_SABER_URL = 'https://scoresaber.com/'
BEAT_SAVER_API_URL = 'https://beatsaver.com/api/maps/detail'
MAX_SONG_QUANTITY = 200


def get_songs_json():
    session = requests.Session()
    set_cookies(session)

    songs = []
    for url in get_song_urls(session):
        bsaber_id = get_bsaber_id(session, url)
        song_data = query_beat_saver_api(session, bsaber_id)
        songs.append(song_data)

    print(json.dumps(songs))


def get_bsaber_id(session, url):
    request_result = session.get(f'https://scoresaber.com/{url}')
    soup = BeautifulSoup(request_result.content, 'html.parser')

    comments = soup.find_all(string=lambda text: isinstance(text, Comment))
    for comment in comments:
        if 'bsaber' not in comment:
            continue
        soup = BeautifulSoup(comment, 'html.parser')
        href = soup.a['href']
        href_parts = href.split('/')
        return href_parts[len(href_parts) - 1]


def query_beat_saver_api(session, bsaber_id):
    request_result = session.get(f'{BEAT_SAVER_API_URL}/{bsaber_id}')
    song_data = json.loads(request_result.content)
    return {'hash': song_data['hash'], 'key': song_data['key'], 'songName': song_data['name']}


def get_song_urls(session):
    page_index = 1
    song_quantity = 0

    while song_quantity < MAX_SONG_QUANTITY:
        request_result = session.get(f'{SCORE_SABER_URL}/?page={page_index}')
        page_index = page_index + 1

        songs_href = extract_songs_href(request_result.content)
        for song_href in songs_href:
            if song_quantity == MAX_SONG_QUANTITY:
                break
            song_quantity = song_quantity + 1
            yield song_href


def extract_songs_href(html):
    soup = BeautifulSoup(html, 'html.parser')
    for td in soup.find_all("td", "song"):
        href = td.a['href']
        yield href


def set_cookies(session):
    session.cookies.set('ranked', '1', domain='scoresaber.com', path='/')
    session.cookies.set('cat', '3', domain='scoresaber.com', path='/')


if __name__ == "__main__":
    get_songs_json()
